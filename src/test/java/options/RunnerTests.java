package options;

import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {"pretty", "html:target/cucumber-reports.html"},
		glue = {"stepdefs"},
		features = {"src/test/features"},
		tags= "@userNameJob")
public class RunnerTests {}
