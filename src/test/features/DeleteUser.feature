Feature: Delete a user validating different scenarios
@userToEliminate
Scenario: A user is eliminated successfully
	Given A ID "2" user to eliminate
	When The user is eliminated
	Then The response status code is 204

@userToEliminateStatusCode
Scenario: A user is eliminated validating the status code
	Given A ID "2" user to eliminate
	When The user is eliminated
	Then The response status code is 201
