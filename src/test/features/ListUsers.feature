Feature: Get the users list validating different scenarios
@userListNameJob
Scenario: Get a complete users list with name and job successfully
	Given The user wants to get a complete users list
	When The user sends the request
	Then The user gets the complete users list with the response code 200
	And The response includes the following
		| page 					| 1             			|
		| per_page 				| 6         			    |

@userListStatusCode
Scenario: Get a complete users list validating the response status code
	Given The user wants to get a complete users list
	When The user sends the request
	Then The user gets the complete users list with the response code 201
	And The response includes the following
		| page 					| 1             			|
		| per_page 				| 6         			    |

@userListContentResponse
Scenario: Get a complete users list validating the response body text
	Given The user wants to get a complete users list
	When The user sends the request
	Then The user gets the complete users list with the response code 200
	And The response includes the following
		| page 					| test             			|
		| per_page 				| test         			    |

