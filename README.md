# README #

Project name: appgaterepo

### Project basic information ###

* In this Git repository is stored the source code of the testing appgate project
* Version 1.0

### Technical information and configuration project ###

1. Source code structure:
    * src/test/features: In this folder are located the created features files
      in gherkin languague 
    * src/test/java
        * /options: In this folder is located the runner test file named "RunnerTests"
        * /stepdefs: In this folder is located the step definition file named "StepDefinitions"
2. Were created 5 features files in gherking languague for the next scenarios:
    * Users creation: 3 scenarios
    * Users deletion: 2 scenarios
    * Users updating: 3 scenarios
    * Users list: 3 scenarios
    * Search for a single user: 3 scenarios
	* Note: The created scenarios are oriented to the succesfull or failure, that means some scenarios will be sucessfull and others will failure depending of the oriented execution
3. For downloading the dependencies was created a build.gradle file in the project root
4. For executing in a locally way there is a file in the next location:
    * src/test/java/options/RunnerTest: The scenarios could be executed by using @tags in CucumberOptions in case we do not want to execute all the scenarios in the other hand we could execute all the scenarios removing the @tags from CucumberOptions section
5. Git repository: Was created a public git repository by using bitbucket with this command: git clone https://luferma01@bitbucket.org/luferma01/appgaterepo.git
6. Execution reports: After executing the scenarios we can find the report in the next path: /target/cucumber-reports.html, in the report can be visualized all the executed scenarios with the steps and sent values