Feature: Get a single user validating different scenarios
@getSingleUser
Scenario: Get a single user by ID successfully
	Given The user wants to get a single user by ID "2"
	When The user sends the request
	Then The user gets the complete single user information with the response code 200
	And The response includes the following
		| data.id 					| 2             			|

@getSingleUserStatusCode
Scenario: Get a single user by ID validating the response status code
	Given The user wants to get a single user by ID "2"
	When The user sends the request
	Then The user gets the complete single user information with the response code 201
	And The response includes the following
		| data.id 					| 2             			|

@getSingleUserContentResponse
Scenario: Get a single user by ID validating the response body text
	Given The user wants to get a single user by ID "2"
	When The user sends the request
	Then The user gets the complete single user information with the response code 200
	And The response includes the following
		| data.id 					| 1             			|