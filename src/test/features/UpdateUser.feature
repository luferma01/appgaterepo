Feature: Update a created user validating different scenarios
@updateUser
Scenario: A user is updated with name and job successfully
	Given A user is updated with name "morpheus" and job "leader"
	When The user is updated
	Then The response status code is 200
	And The response includes the following
		| name 					| morpheus       			|
		| job 					| leader         			|

@updateUserStatusCode
Scenario: A user is updated validating the response status code
	Given A user is updated with name "morpheus" and job "leader"
	When The user is updated
	Then The response status code is 201
	And The response includes the following
		| name 					| morpheus       			|
		| job 					| leader         			|

@updateUserContentResponse
Scenario: A user is updated validating the response body content
	Given A user is updated with name "morpheus" and job "leader"
	When The user is updated
	Then The response status code is 200
	And The response includes the following
		| name 					| test       			|