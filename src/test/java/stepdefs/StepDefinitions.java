package stepdefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;

public class StepDefinitions {

	private Response response;
	private ValidatableResponse json;
	private RequestSpecification request;

	private String ENDPOINT_MAIN_URL = "https://reqres.in/";

	@Given("^A new user with name \"([^\"]*)\" and job \"([^\"]*)\"$")
	public void a_new_user_with_name_and_job_is_created(String name, String job){
		RestAssured.baseURI =ENDPOINT_MAIN_URL;
		request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("job", job);
		request.header("Content-Type", "application/json");
		request.body(requestParams.toString(1));
	}

	@Given("^A ID \"([^\"]*)\" user to eliminate$")
	public void id_user_to_eliminate(String id){
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("id", id);
		request = given().params(parameters);
	}

	@Given("^A user is updated with name \"([^\"]*)\" and job \"([^\"]*)\"$")
	public void a_user_is_updated_with_name_and_job_is_created(String name, String job){
		RestAssured.baseURI =ENDPOINT_MAIN_URL;
		request = RestAssured.given();
		JSONObject requestParams = new JSONObject();
		requestParams.put("name", name);
		requestParams.put("job", job);
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("id", "2");
		request = given().params(parameters);
		request.header("Content-Type", "application/json");
		request.body(requestParams.toString(1));
	}

	@Given("^The user wants to get a complete users list$")
	public void the_user_wants_to_get_a_complete_users_list(){
		RestAssured.baseURI =ENDPOINT_MAIN_URL;
		request = RestAssured.given();
		request.header("Content-Type", "application/json");
	}

	@Given("^The user wants to get a single user by ID \"([^\"]*)\"$")
	public void a_user_wants_to_get_a_single_user_information(String Id){
		RestAssured.baseURI =ENDPOINT_MAIN_URL;
		request = RestAssured.given();
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("id", Id);
		request = given().params(parameters);
		request.header("Content-Type", "application/json");
	}

	@When("^The user is eliminated$")
	public void the_user_is_eliminated(){
		response = request.when().delete(ENDPOINT_MAIN_URL + "api/users/");
		System.out.println("response: " + response.prettyPrint());
	}

	@When("^The new user is created$")
	public void the_new_user_is_created(){
		response = request.when().post("api/users");
		System.out.println("response: " + response.prettyPrint());
	}

	@When("^The user is updated")
	public void the_user_is_updated(){
		response = request.when().patch(ENDPOINT_MAIN_URL + "api/users/");
		System.out.println("response: " + response.prettyPrint());
	}

	@When("^The user sends the request")
	public void the_user_sends_the_request(){
		response = request.when().get(ENDPOINT_MAIN_URL + "api/users/");
		System.out.println("response: " + response.prettyPrint());
	}

	@Then("The response status code is {int}")
	public void verify_status_code(int statusCode)
	{
		json = response.then().statusCode(statusCode);
	}

	@Then("The user gets the complete users list with the response code {int}")
	public void the_user_gets_the_complete_users_list(int statusCode)
	{
		json = response.then().statusCode(statusCode);
	}

	@Then("The user gets the complete single user information with the response code {int}")
	public void the_user_gets_the_single_user_information(int statusCode)
	{
		json = response.then().statusCode(statusCode);
	}

	@And("The response includes the following$")
	public void response_equals(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			if(StringUtils.isNumeric(field.getValue())){
				json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
			}
			else{
				json.body(field.getKey(), equalTo(field.getValue()));
			}
		}
	}

	@And("response includes the following in any order")
	public void response_contains_in_any_order(Map<String,String> responseFields){
		for (Map.Entry<String, String> field : responseFields.entrySet()) {
			try {
				if (StringUtils.isNumeric(field.getValue())) {
					json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
				} else {
					json.body(field.getKey(), containsInAnyOrder(field.getValue()));
				}
			}catch(Exception exc){
				System.out.println("Excepcion: " + exc.getMessage());
			};
		}
	}

}



