Feature: Create a new user validating different scenarios
@userNameJob
Scenario: A new user is created with name and job successfully
	Given A new user with name "morpheus" and job "leader"
	When The new user is created
	Then The response status code is 201
	And The response includes the following
		| name 					| morpheus       			|
		| job 					| leader         			|

@userStatusCode
Scenario: A new user is created validating response status code
	Given A new user with name "morpheus" and job "leader"
	When The new user is created
	Then The response status code is 200
	And The response includes the following
		| name 					| morpheus       			|
		| job 					| leader         			|

@userContentResponse
Scenario: A new user is created validating response body content
	Given A new user with name "morpheus" and job "leader"
	When The new user is created
	Then The response status code is 201
	And The response includes the following
		| name 					| test       			|